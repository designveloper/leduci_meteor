# README #
The quick summary about Meteor and React

### What is this repository for? ###
This repo sets up the basic demo to understand how the Meteor works and use React for View.

### How do I get set up? ###
* Summary of set up
- For editor:
	+ Install some packages below:
		language-babel: support ES6 syntax
		language-javascript-jsx: support JSX for React		
	+ Some useful packages for coding:
		emmet
		eslint (recommend for eslint-config-airbnb)
- For web dev tool:
	+ Install some extensions:
		react Developer Tools: help to see the Virtual DOM
		meteor DevTools
- For project:
	+ Initial Propject:
			meteor create <name_project>
	+ Run project:
			meteor run
			Note: 
			For default, the port is 3000.To change port, meteor run --port <port>
	+ Run mongo:
		meteor run
		Note: Install 3T Studio to see the mongodb database, make sure that check at port 3001 for default
	+ Install packages:
		For node_modules:
			+ Using npm: + meteor npm install --save/--save-dev <name_package> or shorthand meteor npm i -S/-D <name_package>
			+ Using yarn: + yarn add/yarn add -D <name_packages>
	
	For meteor:
		+ meteor add <name_packages>
		+ edit by hand in meteor/packages
		Note: check available package at Atmostpherejs
		
### Meteor notes ###
* View
Meteor supports:
	+ Blaze by default
	+ React
	+ Angular
	
To start new project without Blaze template:

	meteor remove blaze-html-templates
	meteor add static-html 
	
To add React
	yarn add react react-dom
	=> react: support JSX
	=> react-dom: to render Virtual DOM into DOM

* Structure folder:

imports/
--startup/
----client/
-------index.js                 # import client startup through a single index entry point
-------routes.js                # set up all routes in the app
-------useraccounts-configuration.js # configure login templates
----server/
------fixtures.js              # fill the DB with example data on startup
------index.js                 # import server startup through a single index entry point
--api/
----lists/                     # a unit of domain logic
------server/
--------publications.js        # all list-related publications
--------publications.tests.js  # tests for the list publications
------lists.js                 # definition of the Lists collection
------lists.tests.js           # tests for the behavior of that collection
------methods.js               # methods related to lists
------methods.tests.js         # tests for those methods
--ui/
------components/                # all reusable components in the application
                               # can be split by domain if there are many
------layouts/                   # wrapper components for behaviour and visuals
------pages/                     # entry points for rendering used by the router
client/
--main.js                      # client entry point, imports all client code
server/
--main.js                      # server entry point, imports all server code

* Data
Meteor supports MongoDB as a database.
- To create Collection:
	+ Install packages:
		meteor add aldeed:collection2-core
	=> aldeed:collection2-core: support create collection
	
	+ To use data in React
		Install: 
			meteor add react-meteor-data
			yarn add react-addons-pure-render-mixin
			
	+ Syntax: new Mongo.collection(<name_collection>); (Note: import { Mongo } from 'meteor/mongo'
	
- To create Schema:
	+ Install package:
		yarn add simpl-schema
	=> To defind Schema
	+ Checkout some options of SimpleSchema : type/defaultVlue/optional
	+ allow/deny some function support from SimpleSchema: insert/update/remove
	+ Syntax:
		Define Schema: const schema = new SimpleSchema({...})
		Attach Schema into Collection: <name_collection>.attachSchema(<schema>)
		
* Flow pulication and data loading
The problem is 
	+ how Client and Server sync the database changes
	+ how Server protect the direct interactive data from client
Meteor create the reactivity system between the server and client
- Meteor provide publication and methods
- Server will publish the collection (the resource) for client, client need to subscribe to get the data
- For security, Server provides methods (the key) for client, when client wants to implement something on database. It must be acted through the methods which the server provides.
This helps server manage the direct action to database and when the database changes, client will know the these changes through the subcribe

TODO:
- Remove some packages by default:
	meteor remove insecure
	meteor remove autopublish

- To publish collection:
	Meteor.publish('<name_collection>', function() {
    	return <name_collection>.find(); // just syntax
   });

- To subscribe collection:
	Meteor.subcribe('<name_collection>')

- For using Meteor's data system with React
	+ To integrate the two systems, using a react-meteor-data package which allows React components to respond to data changes via Meteor's Tracker reactivity system.
	+ Install package:
		meteor npm install --save react-addons-pure-render-mixin
		meteor add react-meteor-data
	+ Syntax:
		withTracker(() => {
		  Meteor.subscribe('<name_collection>');
		  return {
			<data>
		  };
		})(<Component>);

* Routing
Meteor support:
	+ React-router (recommend for react)
	+ Flow Router
	
* Users and Account
	https://guide.meteor.com/accounts.html#core-meteor

### END ###
The Author: I Le