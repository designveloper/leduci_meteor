import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import injectTapEventPlugin from 'react-tap-event-plugin';

import createBrowserHistory from 'history/createBrowserHistory'
import { Router, Route, Switch } from 'react-router-dom';
import App from '../imports/ui/App';
import NotFound from '../imports/ui/NotFound';
import NewSong from '../imports/ui/NewSong';

const history = createBrowserHistory();
injectTapEventPlugin();

const render = (
  <Router history={history}>
    <Switch>
      <Route exact path="/" component={App} />
      <Route path="/new" component={NewSong} />
      <Route path="*" component={NotFound} />
    </Switch>
  </Router>
);
Meteor.startup(() => {
  ReactDOM.render(render, document.querySelector('#root'));
});

