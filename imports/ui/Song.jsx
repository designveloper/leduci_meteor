import React, { Component } from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import { blue200, blue900 } from 'material-ui/styles/colors';
import PropTypes from 'prop-types';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import classNames from 'classnames';
import { Meteor } from 'meteor/meteor';

const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    margin: 12,
  },
  cardtext: {
    color: 'black',
    marginBottom: 0,
  },
  lyrics: {
    textTransform: 'uppercase',
  },
  iconStyle: {

  },
};

class Song extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDislike: false,
      isLike: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.song && nextProps.song._id !== this.props.song._id) {
      let oldSong = this.props.song;
      if (!Object.keys(this.props.song).length) {
        oldSong = nextProps.song;
      } 
      const song = Object.assign({}, oldSong,
        {
          views: oldSong.views + 1
        }
      );
      Meteor.call('updateSong', song);

      this.setState({
        isDislike: false,
        isLike: false,
      });
    }
  }

  showEditForm() {
    this.props.showEditForm();
  }

  renderHTMLIframe() {
    return { __html: `<iframe width='100%' height='500' src=${this.props.song.url} frameBorder='0' allowFullScreen></iframe>`};
  }

  onLikeClick = () => {
    const { likes, dislikes } = this.props.song;
    const song = Object.assign({}, this.props.song, {
      likes: likes + 1,
      dislikes: this.state.isDislike ? (dislikes ? dislikes  - 1 : 0 ) : dislikes,
    });

    this.setState({
      isLike: true,
      isDislike: false,
    });

    Meteor.call('updateSong', song);
  }

  onDislikeClick = () => {
    const { likes, dislikes } = this.props.song;
    const song = Object.assign({}, this.props.song, {
      likes: this.state.isLike ? (likes ? likes  - 1 : 0 ) : likes,
      dislikes: dislikes + 1,
    });

    Meteor.call('updateSong', song);

    this.setState({
      isLike: false,
      isDislike: true,
    });
  }

  onShareClick = () => {
    window.FB.ui({
      method: 'share',
      display: 'popup',
      href: 'http://localhost:3000/',
    }, function(response){});
  }

  render() {
    return (
      <Card>
        <div dangerouslySetInnerHTML={this.renderHTMLIframe()} />
        <CardHeader>
          <h4>{this.props.song.name}</h4>
          <IconButton tooltip="Like">
            <FontIcon 
              className={classNames({"fa fa-heart-o": !this.state.isLike, "fa fa-thumbs-up": this.state.isLike })} 
              onClick={this.onLikeClick}
            />
          </IconButton>
          <IconButton tooltip="Dislike">
            <FontIcon 
              className={classNames({"fa fa-thumbs-o-down": !this.state.isDislike, "fa fa-thumbs-down": this.state.isDislike })}
              onClick={this.onDislikeClick} 
            />
          </IconButton>
          <IconButton tooltip="Share">
            <FontIcon 
              className="fa fa-share-alt" 
              onClick={this.onShareClick}
            />
          </IconButton>
        </CardHeader>
        <CardText>
          <Chip
            backgroundColor={blue200}
            style={styles.chip}
          >
            Singer: {this.props.song.singer}
          </Chip>
          <Chip
            backgroundColor={blue200}
            style={styles.chip}
          >
            Length: {this.props.song.length}
          </Chip>
        </CardText>
        <CardText style={styles.cardtext}>
          <div className="song__lyrics" style={styles.lyrics}>Lyrics</div>  
          {this.props.song.lyrics}
        </CardText>
        <CardActions>
          <RaisedButton
            label="Edit song" 
            labelPosition="before"
            style={styles.button}
            onClick={this.showEditForm.bind(this)}
          />
        </CardActions>
      </Card>
    );
  }
}

Song.defaultProps = {
  song: PropTypes.shape({
    name: PropTypes.String,
    album: PropTypes.String,
    singer: PropTypes.String,
    length: PropTypes.String,
    lyrics: PropTypes.String,
    url: PropTypes.String,
  }),
  showEditForm: PropTypes.func.isRequired,
};

Song.propTypes = {
  song: {
    length: '',
    name: '',
    lyrics: '',
    url: '',
    album: '',
  },
};

export default Song;
