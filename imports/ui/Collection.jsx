import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import PropTypes from 'prop-types';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';

const styles = {

};

class AlbumList extends Component {
  onItemClick(song) {
    this.props.onSongSelect(song);
  }

  deleteSong = (songId) => {
    Meteor.call('deleteSong', songId, (err) => {
      if (err) {
        alert('Oops! Something went wrong!');
      } else {
        
      }
    });
  }

  render() {
    return (
      <div>
        <h4>PlayList</h4><Link to="/new" className="waves-effect waves-light btn">Add Song</Link>
        <List>
          {this.props.songs.length ? this.props.songs.map(song =>
            (<ListItem
              onClick={() => this.onItemClick(song)}
              key={song._id}
              primaryText={song.name}
              secondaryText={song.length}
              // leftAvatar={<Avatar src="images/ok-128.jpg" />}
              rightIcon={<ActionDelete onClick={this.deleteSong.bind(this, song._id)}/>}
            />),
          ) : <div />
          }
        </List>
      </div>
    );
  }
}

AlbumList.propTypes = {
  songs: PropTypes.array,
  onSongSelect: PropTypes.func.isRequired,
};


AlbumList.defaultProps = {
  songs: [],
};

export default AlbumList;
