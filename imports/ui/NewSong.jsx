import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Songs } from '../api/songs';

class NewSong extends Component {
  submitSong(event) {
    event.preventDefault();

    const song = {
      name: this.refs.name.value,
      singer: this.refs.singer.value,
      album: this.refs.album.value,
      lyrics: this.refs.lyrics.value,
      length: this.refs.length.value,
      url: this.refs.url.value,
      createdAt: new Date(),
      owner: Meteor.userId(),
    };

    console.log(song);
    
    Meteor.call('insertSong', song);

    console.log('add song');
    this.props.history.push('/');
  }

  render() {
    return (
      <div className="row">
        <div className="row">
          <form className="col s12" onSubmit={this.submitSong.bind(this)}>
            <h3>Add a new song</h3>
            <div className="row">
              <div className="input-field col s6">
                <input placeholder="Name" ref="name" type="text" className="validate" />
              </div>
              <div className="input-field col s3">
                <input placeholder="Singer" ref="singer" type="text" className="validate" />
              </div>
              <div className="input-field col s3">
                <input placeholder="Name of Album" ref="album" type="text" className="validate" />
              </div>
            </div>
            <div className="row">
              <div className="input-field col s6">
                <input placeholder="Length" ref="length" type="text" className="validate" />
              </div>
              <div className="input-field col s6">
                <input placeholder="URL" ref="url" type="text" className="validate" />
              </div>
              <div className="input-field col s12">
                <textarea placeholder="Lyrics" ref="lyrics" type="text" className="materialize-textarea" />
              </div>
              <div className="input-field col s12">
                <button type="submit" className="btn waves-effect waves-light" name="action">
                  <i className="material-icons right">Send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default NewSong;
