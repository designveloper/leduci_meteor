import React, { Component } from 'react';

class NotFound extends Component {
  render() {
    return (
      <div>
        Sorry! Maybe your page is not exist. Please try again.
      </div>
    );
  }
}

export default NotFound;