import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Songs } from '../api/songs';

class EditSong extends Component {
  showCollectionStats() {
    this.props.showCollectionStats();
  }

  submitSong(event) {
    event.preventDefault();

    const song = {
      _id: this.props.song._id,
      name: this.refs.name.value,
      singer: this.refs.singer.value,
      album: this.refs.album.value,
      lyrics: this.refs.lyrics.value,
      length: this.refs.length.value,
      url: this.refs.url.value,
      createdAt: new Date(),
      owner: Meteor.userId(),
    };

    Meteor.call('updateSong', song, (err) => {
      if (err) {
        alert('Oops! Something went wrong!');
      } else {
        alert('Song updated');
        this.showCollectionStats();
      }
    });

    this.props.history.push('/');
  }

  render() {

    const { name, singer, album, lyrics, length, url } = this.props.song;

    return (
      <div className="row">
        <div className="row">
          <form className="col s12" onSubmit={this.submitSong.bind(this)}>
            <h3>Edit Song</h3>
            <div className="row">
              <div className="input-field col s6">
                <input placeholder="Name" ref="name" type="text" className="validate" defaultValue={name} />
              </div>
              <div className="input-field col s3">
                <input placeholder="Singer" ref="singer" type="text" className="validate" defaultValue={singer} />
              </div>
              <div className="input-field col s3">
                <input placeholder="Name of Album" ref="album" type="text" className="validate" defaultValue={album} />
              </div>
            </div>
            <div className="row">
              <div className="input-field col s6">
                <input placeholder="Length" ref="length" type="text" className="validate" defaultValue={length} />
              </div>
              <div className="input-field col s6">
                <input placeholder="URL" ref="url" type="text" className="validate" defaultValue={url} />
              </div>
              <div className="input-field col s12">
                <textarea placeholder="Lyrics" ref="lyrics" type="text" className="materialize-textarea" defaultValue={lyrics} />
              </div>
              <div className="input-field col s12">
                <button type="submit" className="btn waves-effect waves-light" name="action">
                  <i className="material-icons right">Send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

EditSong.propTypes = {
  song: PropTypes.shape({
    name: PropTypes.String,
    album: PropTypes.String,
    singer: PropTypes.String,
    length: PropTypes.String,
    lyrics: PropTypes.String,
    url: PropTypes.String,
  }),
  showCollectionStats: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

EditSong.defaultProps = {
  song: {
    name: '',
    album: '',
    singer: '',
    length: '',
    lyrics: '',
    url: '',
  },
};

export default EditSong;
