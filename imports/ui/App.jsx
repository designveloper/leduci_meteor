import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import PropTypes from 'prop-types';
import Collection from './Collection';
import CollectionStats from './CollectionStats';
import Song from './Song';

import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router';
import { withTracker } from 'meteor/react-meteor-data';
import { Songs } from '../api/songs/songs';

import AccountsWrapper from './AccountsWrapper'; 
import EditSong from './EditSong';

import Divider from 'material-ui/Divider';

// const muiTheme = getMuiTheme({
//   palette: {
//     textColor: '#ffffff',
//   },
//   appBar: {
//     height: 50,
//   },
// });

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      currentSong: {},
      showEditSong: false,
    };

    this.showEditForm = this.showEditForm.bind(this);
    this.showCollectionStats = this.showCollectionStats.bind(this);
  }

  handleSongSelect = (song) => {
    this.setState({
      currentSong: song
    });
  }

  showEditForm() {
    this.setState({
      showEditSong: !this.state.showEditSong,
    });
  }

  showCollectionStats() {
    this.setState({
      showEditSong: false,
    });
  }

  showForm() {
    if (this.state.showEditSong) {
      return <EditSong 
          song={this.state.currentSong} 
          showCollectionStats={this.showCollectionStats}
          history={this.props.history}
        />
    }

    return <CollectionStats songs={this.props.songs} />
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.songs.length) {
      if (!Object.keys(this.state.currentSong).length) {
        this.setState({
          currentSong: nextProps.songs[0],
        });
      }
    }
  }

  render() {
    return (
      <MuiThemeProvider > 
        <div className="container">
          <AppBar 
            title="Music Application"
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            showMenuIconButton={false}
          >
            <AccountsWrapper />
          </AppBar>
          <div className="row">
            <div className="col s12 m7"><Song song={this.state.currentSong} showEditForm={this.showEditForm} /></div>
            <div className="col s12 m5"><Collection songs={this.props.songs} onSongSelect={this.handleSongSelect}/></div>
          </div>
          <div className="row">
            <div className="col s12">
              <br/>
              <Divider />
              {this.showForm()}
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  songs: PropTypes.array.isRequired,
};

export default AppContainer = withTracker(() => {
  Meteor.subscribe('songs');
  const user = Meteor.userId();

  return {
    //songs: Songs.find({owner: user}, {sort: {name: 1}}).fetch(),
    songs: Songs.find({}, {sort: {name: 1}}).fetch(),
  };
})(App);