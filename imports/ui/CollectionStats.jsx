import React, { Component } from 'react';
import { Radar } from 'react-chartjs-2';
import Divider from 'material-ui/Divider';
import PropTypes from 'prop-types';

class CollectionStats extends Component {
  render() {
    const songs = this.props.songs;

    const total = songs.reduce((acc, next) => {
      acc.views += next.views;
      acc.likes += next.likes;
      acc.dislikes += next.dislikes;
      acc.shares += next.shares;
      acc.comments += next.comments ? next.comments.length : 0;
      return acc;
    }, Object.create({
      views: 0,
      likes: 0,
      dislikes: 0,
      shares: 0,
      comments: 0,
    }));

    const data = {
      labels: ['View', 'Like', 'Dislike', 'Share', 'Comment'],
      datasets: [
        {
          label: 'Total',
          backgroundColor: 'rgba(143, 202, 249, 0.2)',
          borderColor: 'rgba(12, 71, 161, 1)',
          pointBackgroundColor: 'rgba(12, 71, 161, 1)',
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(12, 71, 161, 1)',
          data: [
            total.views,
            total.likes,
            total.dislikes,
            total.shares,
            total.comments,
          ],
        },
      ],
    };

    return (
      <div>
        <h2>Collection Stats</h2>
        <div className="row">
          <div className="col s12 m7">
            <Radar
              data={data}
              width={500}
              height={500}
              option={{
                maintainApspectRadio: false,
              }}
            />
          </div>
          <Divider />
          <div className="col s12 m5">
            <h4>View total: {total.views}</h4>
          </div>
        </div>
      </div>
    );
  }
}

CollectionStats.propTypes = {
  songs: PropTypes.array,
};


CollectionStats.defaultProps = {
  songs: [],
};

export default CollectionStats;
