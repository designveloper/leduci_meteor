import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Songs } from './songs';

Meteor.methods({
  insertSong(song) {
    check(song, Object);

    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }
    Songs.insert(song);
  },
  updateSong(song) {
    check(song, Object);

    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized');
    }

    Songs.update(song._id, { $set: song });
  },
  deleteSong(songId) {
    check(songId, String);

    const song = Songs.findOne(songId);

    if (Meteor.userId() !== song.owner) {
      alert("You don't have permission to delete this song");
    } else {
      Songs.remove(songId);
    }
  },
});
