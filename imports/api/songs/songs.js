import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

export const Songs = new Mongo.Collection('songs');

const SongSchema = new SimpleSchema({
  name: { type: String, required: true },
  singer: { type: String, required: true },
  length: { type: String, required: true },
  lyrics: { type: String, defaultValue: '' },
  dislikes: { type: Number, defaultValue: 0 },
  likes: { type: Number, defaultValue: 0 },
  views: { type: Number, defaultValue: 0 },
  shares: { type: Number, defaultValue: 0 },
  comments: { type: Array, optional: true },
  url: { type: String, required: true },
  album: { type: String, optional: true },
  owner: { type: String, optional: true },
});

Songs.attachSchema(SongSchema);

Songs.allow({
  insert() { return false; },
  update() { return false; },
  remove() { return false; },
});

Songs.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

if (Meteor.isServer) {
  const data = [
    {
      name: 'Sugar',
      singer: 'Maroon 5',
      lyrics: 'I miss the taste of a sweeter life',
      length: '3:28',
      url: 'https://www.youtube.com/embed/NmugSMBh_iI',
      createdAt: new Date(),
    },
    {
      name: 'Map',
      singer: 'Maroon 5',
      lyrics: "I'm hurting, baby, I'm broken down",
      length: '5:02',
      url: 'https://www.youtube.com/embed/09R8_2nJtjg',
      createdAt: new Date(),
    },
    {
      name: 'One More Night',
      singer: 'Maroon 5',
      lyrics: "You and I go hard at each other like we're going to war. But I'll only stay with you one more night",
      length: '3:42',
      url: 'https://www.youtube.com/embed/fwK7ggA3-bU',
      createdAt: new Date(),
    },
    {
      name: 'Payphone (Explicit) ft. Wiz Khalifa',
      singer: 'Maroon 5',
      lyrics: "Payphone I'm at a payphone trying to call home",
      length: '4:39',
      url: 'https://www.youtube.com/embed/KRaWnd3LJfs',
      createdAt: new Date(),
    },
  ];

  if (!Songs.find({}).count()) {
    data.forEach((song) => {
      Songs.insert(song);
    });
  }
}