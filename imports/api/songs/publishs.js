import { Songs } from './songs';
import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
  Meteor.publish('songs', () => {
    // if (!this.userId) return;

    // return Songs.find({ownerId: this.userId });

    return Songs.find({});
  });
}
